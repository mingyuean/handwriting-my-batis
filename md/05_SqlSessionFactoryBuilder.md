# SqlSessionFactoryBuilder

[Mybatis深入源码分析之SqlSessionFactoryBuilder](https://blog.csdn.net/qq_36617322/article/details/90903926)

[Mybatis源码之美1.4.SqlSessionFactory对象的创建工作 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/146061404)

每一个MayBatis应用程序都是以一个SqlSessionFactory对象的实例为核心。

SqlSessionFactory对象的实例可以听过SqlSessionFactoryBuilder对象来获取。

SqlSessionFactoryBuilder对象可以从XML配置文件中加载配置信息，并根据配置信息创建SqlSessionFactory。

SqlSessionFactoryBuilder提供了三种途径去读取MyBatis配置文件

[点击了解MyBatis多环境配置](https://mybatis.org/mybatis-3/zh/configuration.html#environments)

[点击了解MyBatis属性配置](https://mybatis.org/mybatis-3/zh/configuration.html#properties)

##### 源码解析+注释
```java
public class SqlSessionFactoryBuilder {
    // reader：配置文件对应的字节流
    public SqlSessionFactory build(Reader reader) {
        return build(reader, null, null);
    }
    // reader：配置文件对应的字符流
    // environment：指定当前使用的环境标志，配置文件中的environment标签的id
    public SqlSessionFactory build(Reader reader, String environment) {
        return build(reader, environment, null);
    }
    // reader：配置文件对应的字符流
    // properties：用户自定义的属性对象，可在全局配置文件和mapper配置文件中以占位符的形式使用
    public SqlSessionFactory build(Reader reader, Properties properties) {
        return build(reader, null, properties);
    }
    // 字符流主要执行的方法
    // reader：配置文件对应的字符流
    // environment：指定当前使用的环境标志，配置文件中的environment标签的id
    // properties：用户自定义的属性对象，可在全局配置文件和mapper配置文件中以占位符的形式使用
    public SqlSessionFactory build(Reader reader, String environment, Properties properties) {
        try {
            // XMLConfigBuilder是对MyBatis的配置文件进行解析的类
            XMLConfigBuilder parser = new XMLConfigBuilder(reader, environment, properties);
            return build(parser.parse());
        } catch (Exception e) {
            // 生成SqlSession时出错
            throw ExceptionFactory.wrapException("Error building SqlSession.", e);
        } finally {
            ErrorContext.instance().reset();
            try {
                reader.close();
            } catch (IOException e) {
                // 故意忽略。首选上一个错误。
                // Intentionally ignore. Prefer previous error.
            }
        }
    }
    // reader：配置文件对应的字节流
    public SqlSessionFactory build(InputStream inputStream) {
        return build(inputStream, null, null);
    }
    // inputStream：配置文件对应的字节流
    // environment：指定当前使用的环境标志，配置文件中的environment标签的id
    public SqlSessionFactory build(InputStream inputStream, String environment) {
        return build(inputStream, environment, null);
    }
    // inputStream：配置文件对应的字节流
    // properties：用户自定义的属性对象，可在全局配置文件和mapper配置文件中以占位符的形式使用
    public SqlSessionFactory build(InputStream inputStream, Properties properties) {
        return build(inputStream, null, properties);
    }
    // 字节流主要执行的方法
    // inputStream：配置文件对应的字节流
    // environment：指定当前使用的环境标志，配置文件中的environment标签的id
    // properties：用户自定义的属性对象，可在全局配置文件和mapper配置文件中以占位符的形式使用
    public SqlSessionFactory build(InputStream inputStream, String environment, Properties properties) {
        try {
            // XMLConfigBuilder是对MyBatis的配置文件进行解析的类
            XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
            return build(parser.parse());
        } catch (Exception e) {
            // 生成SqlSession时出错
            throw ExceptionFactory.wrapException("Error building SqlSession.", e);
        } finally {
            ErrorContext.instance().reset();
            try {
                inputStream.close();
            } catch (IOException e) {
                // 故意忽略。首选上一个错误。
                // Intentionally ignore. Prefer previous error.
            }
        }
    }
    // 直接使用org.apache.ibatis.session.Configuration配置类
    public SqlSessionFactory build(Configuration config) {
        return new DefaultSqlSessionFactory(config);
    }

}
```

