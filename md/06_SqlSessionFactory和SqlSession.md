[SqlSessionFactory和SqlSession详解](https://blog.csdn.net/qq_40776361/article/details/125086549)

# SqlSessionFactory

SqlSessionFactory是MyBatis的关键对象,它是个单个数据库映射关系经过编译后的内存镜像。

1. SqlSessionFactory是MyBatis的关键对象,它是个单个数据库映射关系经过编译后的内存镜像。

2. SqlSessionFactory对象的实例可以通过SqlSessionFactoryBuilder对象类获得,而SqlSessionFactoryBuilder则可以从XML配置文件或一个预先定制的Configuration的实例构建出SqlSessionFactory的实例。

3. 每一个MyBatis的应用程序都以一个SqlSessionFactory对象的实例为核心。

4. SqlSessionFactory是线程安全的,SqlSessionFactory一旦被创建,应该在应用执行期间都存在。在应用运行期间不要重复创建多次,建议使用单例模式。

5. SqlSessionFactory是创建SqlSession的工厂。

# SqlSession

1. SqlSession是MyBatis的关键对象,是执行持久化操作的独享,类似于JDBC中的Connection。

2. 它是应用程序与持久层之间执行交互操作的一个单线程对象,也是MyBatis执行持久化操作的关键对象。

3. SqlSession对象完全包含以数据库为背景的所有执行SQL操作的方法,它的底层封装了JDBC连接,可以用SqlSession实例来直接执行被映射的SQL语句。

4. 每个线程都应该有它自己的SqlSession实例。

5. SqlSession的实例不能被共享,同时SqlSession也是线程不安全的,绝对不能讲SqlSession实例的引用放在一个类的静态字段甚至是实例字段中。也绝不能将SqlSession实例的引用放在任何类型的管理范围中,比如Servlet当中的HttpSession对象中。

6. 使用完SqlSession之后关闭Session很重要,应该确保使用finally块来关闭它。






