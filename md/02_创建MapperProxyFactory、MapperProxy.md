# 创建MapperProxyFactory、MapperProxy

#### 基础测试

**数据库表**

```sql
-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NULL DEFAULT NULL,
  `activity_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `activity_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES (1, 100001, '活动', '描述', '2023-03-12 01:07:02', '2023-03-22 01:07:06');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `userHead` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '1', '张三', '头像', '2023-03-12 01:06:41', '2023-03-15 01:06:45');
```

**实体类**

```java
public class User {
    /**
     * 主键
     */
    private Long id;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 头像
     */
    private String userHead;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userHead='" + userHead + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
    // 省略get和set方法
}
```

**Mapper接口**

```java
public interface UserMapper {
    
    /**
     * 查询全部用户
     */
    @Select("select * from user")
    List<User> findAllUser();

    /**
     * 通过id查询用户
     */
    @Select("select * from user where id = #{id} ")
    User findUserById(Integer id);


    /**
     * 通过userId和userName查询用户
     */
    @Select("select * from user where userId = #{userId} and userName = #{userName}")
    User findUserByUsernamePassword(@Param("userId") String userId, @Param("userName") String userName);
}
```

**Mapper配置**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.mingyuean.demo.test.UserMapper">

    <select id="findAllUser" resultType="com.mingyuean.demo.test.User">
        select *
        from user
    </select>

    <select id="findUserById" resultType="com.mingyuean.demo.test.User">
        select *
        from user
        where id = #{id}
    </select>

    <select id="findUserByUsernamePassword" resultType="com.mingyuean.demo.test.User">
        select *
        from user
        where userId = #{userId}
          and userName = #{userName}
    </select>

</mapper>
```

#### MapperProxy：映射器代理

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;

/**
 * @author MingYueAn
 * <p>  映射器代理，实现了InvocationHandler接口
 * <p>  2023/3/12 14:59
 * @version: 1.0
 */
public class MapperProxy<T> implements InvocationHandler {

    private static final Logger log = LoggerFactory.getLogger(MapperProxyFactory.class);

    /**
     * TODO: 2023/3/14 模拟一个SqlSession
     */
    private Map<Object, Object> sqlSessionMap;
    /**
     * 传入的mapperInterface
     */
    private final Class<T> mapperInterfaceClass;

    public MapperProxy(Map<Object, Object> sqlSessionMap, Class<T> mapperInterfaceClass) {
        this.sqlSessionMap = sqlSessionMap;
        this.mapperInterfaceClass = mapperInterfaceClass;
    }

    // proxy一般是指代理类
    // method是被代理的方法
    // args为该方法的参数数组
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.debug("{}---->{}", mapperInterfaceClass.getName(), sqlSessionMap.get(mapperInterfaceClass.getName()));
        //method.getDeclaringClass()：Method对象表示的方法的类的Class对象
        //如果Class是Object.class的话，直接调用该方法，不做其他操作
        //比如：toString()、hashCode()
        if (Object.class.equals(method.getDeclaringClass())) {
            return method.invoke(this, args);
        }
        log.debug("方法名 = {}，参数数组args = {}", method.getName(), Arrays.toString(args));
        // 获取数据库连接---->获取预处理语句---->设置占位符---->执行预处理语句---->获取结果集---->结果集处理
        // 解析sql---->执行sql---->结果处理

        return null;
    }
}
```

#### MapperProxyFactory：映射器代理工厂

```java
//映射器代理工厂：https://blog.csdn.net/qq_37855749/article/details/119544954

/**
 * @author MingYueAn
 * <p>  映射器代理工厂
 * <p>  2023/3/11 21:49
 * @version: 1.0
 */
public class MapperProxyFactory<T> {

    private static final Logger log = LoggerFactory.getLogger(MapperProxyFactory.class);

    /**
     * 接口类:mapper接口
     */
    private final Class<T> mapperInterfaceClass;

    /**
     * 构造方法
     *
     * @param mapperInterfaceClass 接口类:mapper接口类
     */
    public MapperProxyFactory(Class<T> mapperInterfaceClass) {
        log.debug("获取到的mapper接口：{}",mapperInterfaceClass.getName());
        this.mapperInterfaceClass = mapperInterfaceClass;
    }


    /**
     * 新建实例
     */
    public T newInstance(Map<Object, Object> sqlSessionMap) {
        log.debug("{}---->{}", mapperInterfaceClass.getName(), sqlSessionMap.get(mapperInterfaceClass.getName()));
        // 处理器
        final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSessionMap, mapperInterfaceClass);
        // 使用代理，获取实例化对象
        final Object obj = Proxy.newProxyInstance(
                mapperInterfaceClass.getClassLoader(),
                new Class[]{mapperInterfaceClass},
                mapperProxy
        );
        return (T) obj;
    }


}
```

#### @Select注解

```java
/**
 * @author MingYueAn
 * <p>  查询注解
 * <p>  2023/3/11 21:43
 * @version: 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Select {
    /**
     * 查询语句
     * @return 字符串
     */
    String value() default "";
}
```

#### @Param注解

```java
/**
 * @author MingYueAn
 * <p>  参数注解
 * <p>  2023/3/12 12:25
 * @version: 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Param {
    /**
     * 参数
     *
     * @return 字符串
     */
    String value();
}
```

#### 测试1

```java

    /**
     * MapperProxy测试
     */
    @Test
    public void test_MapperProxy() {
        // 被代理的对象
        final Class<UserMapper> userMapperClass = UserMapper.class;
        // 模拟的sqlSession
        final Map<Object, Object> map = new HashMap<>();
        // 处理器
        final MapperProxy<UserMapper> mapperProxy = new MapperProxy<>(map, userMapperClass);

        // 代理模式：https://blog.csdn.net/Passer_hua/article/details/122617628
        /* 代理模式：JDK动态代理 */
        //newProxyInstance: 创建代理对象
        //Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler handler)
        // loader: 被代理类对象
        // interfaces：接口类对象，被代理对象所实现的接口数组
        // handler：调用处理器，被调用对象的那个方法被调用后该如何处理
        final Object newProxyInstance = Proxy.newProxyInstance(
                ClassLoader.getSystemClassLoader(),
                new Class[]{userMapperClass},
                mapperProxy
        );
        // 强制转换
        UserMapper userMapper = (UserMapper) newProxyInstance;
        userMapper.findUserById(1);
    }
```

#### 测试2

```java


    /**
     * MapperProxyFactory测试
     */
    @Test
    public void test_MapperProxyFactory() {
        // 模拟的sqlSession
        final Map<Object, Object> map = new HashMap<>();
        // 被代理的对象
        final Class<UserMapper> userMapperClass = UserMapper.class;
        // Mapper代理工厂
        final MapperProxyFactory<UserMapper> mapperProxyFactory = new MapperProxyFactory<>(userMapperClass);
        // 通过代理工厂实例花代理接口
        final UserMapper userMapper = mapperProxyFactory.newInstance(map);
        userMapper.findUserById(1);
    }

    public void test_MapperRegistry(){
        // 注册Mapper
        final MapperRegistry mapperRegistry = new MapperRegistry();
        // mapper注册表中添加要扫描的包
        mapperRegistry.addMappers("com.mingyuean.demo.test");
    }
```