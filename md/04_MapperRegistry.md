# MapperRegistry

[Mybatis源码分析:MapperRegistry](https://www.cnblogs.com/zhengzuozhanglina/p/11234690.html)

MapperRegistry用于注册，获取和判断是否Mapper接口已经被注册的功能

MapperRegistry 提供包路径的扫描和映射器代理类注册机服务，完成接口对象的代理类注册处理。

#### MapperRegistry

```java
package com.mingyuean.demo.mybatis;

import cn.hutool.core.lang.ClassScanner;
import com.mingyuean.demo.mybatis.binding.MapperProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author MingYueAn
 * <p>  映射器注册表
 * <p>  2023/3/14 13:45
 * @version: 1.0
 */
public class MapperRegistry {

    private static final Logger log = LoggerFactory.getLogger(MapperProxyFactory.class);

    // 存放映射器代理
    // key--->类 value--->映射代理工厂
    private final Map<Class<?>, MapperProxyFactory<?>> mapperProxyFactoryMap = new HashMap<>();

    /**
     * 通过包名扫描Class
     * @param packageName
     */
    public void addMappers(String packageName) {
        // 通过包名扫描
        final Set<Class<?>> mapperClassSet = ClassScanner.scanPackage(packageName);
        // 循环遍历
        for (Class<?> mapperClass : mapperClassSet) {
            addMapper(mapperClass);
        }
    }

    /**
     * 将mapperClass添加到 mapperProxyFactoryMap
     * @param mapperClass
     */
    public void addMapper(Class<?> mapperClass) {
        log.debug("扫描到的类：{}", mapperClass);
        // 判断是否是接口类型
        if (mapperClass.isInterface()) {
            // 判断是否重复添加
            if (hasMapper(mapperClass)) {
                // 抛出异常
                throw new RuntimeException("类型 " + mapperClass + " mapperProxyFactoryMap已存在");
            }
            // 注册映射器代理工厂
            mapperProxyFactoryMap.put(mapperClass, new MapperProxyFactory<>(mapperClass));
            log.debug("存放MapperProxyFactory的集合：{}", mapperProxyFactoryMap);
        }
    }

    /**
     * 判断mapperProxyFactoryMap中是否已经存在mapperClass
     * @param mapperClass
     * @return
     */
    private boolean hasMapper(Class<?> mapperClass) {
        return mapperProxyFactoryMap.containsKey(mapperClass);
    }

    // TODO: 2023/3/15 获取未完成 
}
```

#### 测试

```java
    @Test
    public void test_MapperRegistry(){
        // 注册Mapper
        final MapperRegistry mapperRegistry = new MapperRegistry();
        // mapper注册表中添加要扫描的包
        mapperRegistry.addMappers("com.mingyuean.demo.test");
    }
```

