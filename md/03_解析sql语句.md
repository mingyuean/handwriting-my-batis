# 解析SQL语句

#### MapperProxy：映射器代理，实现了InvocationHandler接口

```java
import com.mingyuean.demo.mybatis.parameter.GenericTokenParser;
import com.mingyuean.demo.mybatis.parameter.ParameterMapping;
import com.mingyuean.demo.mybatis.parameter.ParameterMappingTokenHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author MingYueAn
 * <p>  映射器代理，实现了InvocationHandler接口
 * <p>  2023/3/12 14:59
 * @version: 1.0
 */
public class MapperProxy implements InvocationHandler {

    private static final Logger log = LoggerFactory.getLogger(MapperProxyFactory.class);

    // proxy一般是指代理类
    // method是被代理的方法
    // args为该方法的参数数组
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //method.getDeclaringClass()：Method对象表示的方法的类的Class对象
        //如果Class是Object.class的话，直接调用该方法，不做其他操作
        //比如：toString()、hashCode()
        if (Object.class.equals(method.getDeclaringClass())) {
            return method.invoke(this, args);
        }
        log.debug("方法名 = {}，参数数组args = {}", method.getName(), Arrays.toString(args));
        // 获取数据库连接---->获取预处理语句---->设置占位符---->执行预处理语句---->获取结果集---->结果集处理
        // 解析sql---->执行sql---->结果处理

        // TODO: 2023/3/12  【1】获取数据库连接
        Connection connection = getConnection();

        // 判断方法是否存在Select注解
        if (method.isAnnotationPresent(Select.class)) {
            // 从@Select中获取sql语句
            final String sql = method.getAnnotation(Select.class).value();
            log.debug("未处理SQL语句 = " + sql);
            // 解析Sql语句，需要将 #{id} 解析
            final ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
            GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
            //获取处理后的结果
            String parseSql = genericTokenParser.parse(sql);
            log.debug("处理后的SQL语句 = " + parseSql);
            //获取参数名集合
            final List<ParameterMapping> parameterMappingList = parameterMappingTokenHandler.getParameterMappings();
            //参数名集合
            log.debug("有序的存储sql语句中的参数名集合：{}", String.valueOf(parameterMappingList));
            //----------------------------------------------------------
            // 创建一个存放 参数名:参数值 的集合
            final HashMap<String, Object> parameterNameValueMap = new HashMap<>();
            // 反射：获取方法中全部参数
            final Parameter[] parametersArray = method.getParameters();
            // 循环遍历参数数组
            for (int i = 0; i < parametersArray.length; i++) {
                // 判断是否存在注解@Param
                if (parametersArray[i].isAnnotationPresent(com.mingyuean.demo.mybatis.Param.class)) {
                    // 从注解中获取参数名
                    // 存入容器，key--名称，value--数值
                    parameterNameValueMap.put(parametersArray[i].getAnnotation(com.mingyuean.demo.mybatis.Param.class).value(), args[i]);
                }
                // 获取参数名arg0，获取参数值
                parameterNameValueMap.put(parametersArray[i].getName(), args[i]);
            }
            log.debug("存放【参数名:参数值】的集合：{}", parameterNameValueMap);
            //----------------------------------------------------------
            // 新建一个预处理语句对象
            final PreparedStatement preparedStatement = connection.prepareStatement(parseSql);
            // 将预处理语句中的占位符?用值替换
            for (int i = 0; i < parameterMappingList.size(); i++) {
                // 从集合中获取参数名
                final String content = parameterMappingList.get(i).getContent();
                // 获取参数值的class类型
                // TODO: 2023/3/12 未完成
                final Class<?> typeClass = parameterNameValueMap.get(content).getClass();
                System.out.println("typeClass = " + typeClass);
                // 占位符替换
                preparedStatement.setObject(i + 1, parameterNameValueMap.get(content));
            }
            //----------------------------------------------------------
            // 执行预处理语句
            final boolean execute = preparedStatement.execute();
            if (execute) {
                // 获取结果集
                final ResultSet resultSet = preparedStatement.getResultSet();
            }


        }

        return null;
    }

    // TODO: 2023/3/12 获取连接
    private static Connection getConnection() throws SQLException, ClassNotFoundException {

        // 1.加载驱动
        Class.forName("com.mysql.jdbc.Driver");

        // 2.连接信息
        String url = "jdbc:mysql://localhost:3306/mya-mybatis?useUnicode=true&characterEncoding=utf8&useSSL=false";
        String username = "root";
        String password = "";

        // 3.连接成功
        return DriverManager.getConnection(url, username, password);
    }
}
```

#### 参数处理

**GenericTokenParser**

```java
package com.mingyuean.demo.mybatis.parameter;

/**
 * @author MingYueAn
 * <p>  通用令牌分析器
 * <p>  2023/3/12 17:15
 * @version: 1.0
 */
public class GenericTokenParser {

    /**
     * 开始的标记
     */
    private final String openToken;
    /**
     * 结束的标记
     */
    private final String closeToken;
    /**
     * 处理器
     */
    private final TokenHandler tokenHandler;

    /**
     * 构造器
     *
     * @param openToken    开始标记
     * @param closeToken   结束标记
     * @param tokenHandler 处理器
     */
    public GenericTokenParser(String openToken, String closeToken, TokenHandler tokenHandler) {
        this.openToken = openToken;
        this.closeToken = closeToken;
        this.tokenHandler = tokenHandler;
    }

    /**
     * 解析SQL语句
     *
     * @param sql
     * @return
     */
    public String parse(String sql) {
        final StringBuilder stringBuilder = new StringBuilder();
        // 判断传过来的sql语句是否为null或""
        if (sql != null && sql.length() > 0) {
            //将sql语句转换为char数组
            final char[] sqlChar = sql.toCharArray();
            int offset = 0;
            //获取"#{"第一次出现的位置
            //返回某个指定的字符串值在字符串中首次出现的位置
            int start = sql.indexOf(openToken, offset);
            while (start > -1) {
                if (sqlChar[start - 1] == '\\') {
                    stringBuilder.append(sqlChar, offset, start - offset - 1).append(openToken);
                    offset = start + openToken.length();
                } else {
                    int end = sql.indexOf(closeToken, start);
                    if (end == -1) {
                        stringBuilder.append(sqlChar, offset, sqlChar.length - offset);
                        offset = sqlChar.length;
                    } else {
                        stringBuilder.append(sqlChar, offset, start - offset);
                        offset = start + openToken.length();
                        // 获取#{id}中的id
                        String content = new String(sqlChar, offset, end - offset);
                        // 得到一对大括号里的字符串后，调用handler.handleToken,比如替换变量这种功能
                        stringBuilder.append(tokenHandler.handleToken(content));
                        offset = end + closeToken.length();
                    }
                }
                start = sql.indexOf(openToken, offset);
            }
            if (offset < sqlChar.length) {
                stringBuilder.append(sqlChar, offset, sqlChar.length - offset);
            }
        }
        return stringBuilder.toString();
    }

}
```

**ParameterMapping**

```java
package com.mingyuean.demo.mybatis.parameter;

/**
 * @author MingYueAn
 * <p>  参数映射
 * <p>  2023/3/12 19:54
 * @version: 1.0
 */
public class ParameterMapping {
    private String content;

    @Override
    public String toString() {
        return "ParameterMapping{" +
                "content='" + content + '\'' +
                '}';
    }

    public ParameterMapping(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

```

**ParameterMappingTokenHandler**

```java
package com.mingyuean.demo.mybatis.parameter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MingYueAn
 * <p>  参数映射令牌处理程序
 * <p>  2023/3/12 19:47
 * @version: 1.0
 */
public class ParameterMappingTokenHandler implements TokenHandler {

    private final List<ParameterMapping> parameterMappings = new ArrayList<>();

    @Override
    public String handleToken(String content) {
        parameterMappings.add(new ParameterMapping(content));
        return "?";
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }
}

```

**TokenHandler**

```java
package com.mingyuean.demo.mybatis.parameter;

/**
 * @author MingYueAn
 * <p>  令牌处理程序
 * <p>  2023/3/12 17:17
 * @version: 1.0
 */
public interface TokenHandler {
    /**
     * 处理
     * @param value 值
     * @return ?
     */
    String handleToken(String value);
}

```

#### 类型处理

**TypeHandler<T>**

```java
package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  类型处理器接口
 * <p>  2023/3/12 22:31
 * @version: 1.0
 */
public interface TypeHandler<T> {
    /**
     * 设置参数
     * @param preparedStatement 预处理语句
     * @param index 索引
     * @param value 值
     */
    void setParameter(PreparedStatement preparedStatement, int index, T value) throws SQLException;
}

```

**StringTypeHandler**

```java
package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  字符串类型处理器
 * <p>  2023/3/12 22:38
 * @version: 1.0
 */
public class StringTypeHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int index, String value) throws SQLException {
        preparedStatement.setString(index, value);
    }

}

```