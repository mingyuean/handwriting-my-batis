package com.mingyuean.demo.learn;

/**
 * @author MingYueAn
 * <p>  ClassLoader的getSystemClassLoader（）方法
 * <p>  2023/3/12 2:08
 * @version: 1.0
 */
// Java程序演示示例
// ClassLoader的getSystemClassLoader（）方法
public class GetSystemClassLoader {

    public static void main(String[] args) throws Exception {
        // 它返回附加了给定类名的Class对象
        Class cl = Class.forName("com.mingyuean.demo.learn.GetSystemClassLoader");

        // 它返回附加了给定类名的ClassLoader对象
        ClassLoader loader = cl.getClassLoader();

        // 显示加载器类
        System.out.println(loader.getClass());

        // 它返回附加了给定类名的SystemClassLoader对象
        loader = loader.getSystemClassLoader();

        // 显示SystemClassLoader类
        System.out.println(loader.getClass());
    }
}

//        getSystemClassLoader() 方法可在java.lang包。
//        getSystemClassLoader() 方法用于查找用于委托的 System 类加载器，这将是 ClassLoader 的新实例的默认委托父级。
//        getSystemClassLoader() 方法是一个静态方法，它可以通过类名访问，如果我们尝试使用类对象访问该方法，那么我们不会得到任何错误。
//        getSystemClassLoader() 方法检查安全约束时可能会抛出异常。
//        SecurityException: 在这个异常中，当安全管理器存在时，它的 checkPermission() 方法不允许访问系统类加载器。
//        IllegalStateException:在此异常中，在构造由属性 "java.system.class.loader" 给出的类加载器期间递归调用时
// https://baijiahao.baidu.com/s?id=1675251942025828596&wfr=spider&for=pc
// http://www.manongjc.com/detail/31-fhuelbiskcblwsl.html