package com.mingyuean.demo.test;

import com.mingyuean.demo.mybatis.annotations.Param;

import java.util.List;

/**
 * @author MingYueAn
 * <p>  用户Mapper
 * <p>  2023/3/11 21:37
 * @version: 1.0
 */
public interface UserMapper {

    /**
     * 查询全部用户
     */
//    @Select("select * from user")
    List<User> findAllUser();

    /**
     * 通过id查询用户
     */
//    @Select("select * from user where id = #{id} ")
    User findUserById(Integer id);


    /**
     * 通过userId和userName查询用户
     */
//    @Select("select * from user where userId = #{userId} and userName = #{userName}")
    User findUserByUsernamePassword(@Param("userId") String userId, @Param("userName") String userName);
}
