package com.mingyuean.demo.test;

import java.sql.*;

/**
 * @author MingYueAn
 * <p>  原始的使用JDBC操作数据库
 * <p>  2023/3/12 2:24
 * @version: 1.0
 */
public class JDBCTest {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // 1.加载驱动
        Class.forName("com.mysql.jdbc.Driver");

        // 2.连接信息
        String url = "jdbc:mysql://localhost:3306/mya-mybatis?useUnicode=true&characterEncoding=utf8&useSSL=false";
        String username = "root";
        String password = "";

        // 3.连接成功
        Connection connection = DriverManager.getConnection(url, username, password);

        // 4.执行SQL的对象获取
        Statement statement = connection.createStatement();

        // 5.待执行SQL
        String sql = "SELECT id, userId, userName, userHead FROM user";

        // 6.执行SQL，并输出结果
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            System.out.print("id=" + resultSet.getObject("id") + " ");
            System.out.print("userId=" + resultSet.getObject("userId") + " ");
            System.out.print("userName=" + resultSet.getObject("userName") + " ");
            System.out.print("userHead=" + resultSet.getObject("userHead"));
        }

        // 7.释放连接
        resultSet.close();
        statement.close();
        connection.close();
    }

}

