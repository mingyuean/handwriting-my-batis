package com.mingyuean.demo.test;


import java.util.List;

public interface ActivityMapper {

    List<Activity> findAllActivity();

}
