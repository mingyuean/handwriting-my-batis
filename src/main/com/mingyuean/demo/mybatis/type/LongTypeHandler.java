package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  长整型类型处理器
 * <p>  2023/3/12 23:04
 * @version: 1.0
 */
public class LongTypeHandler implements TypeHandler<Long> {
    @Override
    public void setParameter(PreparedStatement preparedStatement, int index, Long value) throws SQLException {
        preparedStatement.setLong(index, value);
    }

    @Override
    public Long getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getLong(columnName);
    }
}
