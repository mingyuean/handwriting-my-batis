package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  整形类型处理器
 * <p>  2023/3/12 22:41
 * @version: 1.0
 */
public class IntegerTypeHandler implements TypeHandler<Integer>{
    @Override
    public void setParameter(PreparedStatement preparedStatement, int index, Integer value) throws SQLException {
        preparedStatement.setInt(index,value);
    }

    @Override
    public Integer getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getInt(columnName);
    }
}
