package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  字符串类型处理器
 * <p>  2023/3/12 22:38
 * @version: 1.0
 */
public class StringTypeHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int index, String value) throws SQLException {
        preparedStatement.setString(index, value);
    }

    @Override
    public String getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getString(columnName);
    }

}
