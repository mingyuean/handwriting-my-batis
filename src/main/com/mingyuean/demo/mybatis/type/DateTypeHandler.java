package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author MingYueAn
 * <p>
 * <p>  2023/3/15 21:27
 * @version: 1.0
 */
public class DateTypeHandler implements TypeHandler<Date> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int index, Date value) throws SQLException {
        preparedStatement.setTimestamp(index, new Timestamp(value.getTime()));
    }

    @Override
    public Date getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getDate(columnName);
    }
}
