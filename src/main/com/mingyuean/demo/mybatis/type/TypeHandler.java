package com.mingyuean.demo.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  类型处理器接口
 * <p>  2023/3/12 22:31
 * @version: 1.0
 */
public interface TypeHandler<T> {


     /**
     * 设置参数
     * @param preparedStatement 预处理语句
     * @param index 索引
     * @param value 值
     * @throws SQLException SQL异常
     */
    void setParameter(PreparedStatement preparedStatement,int index,T value) throws SQLException;

    /**
     * 获取结果
     * @param resultSet 结果集
     * @param columnName 字段名
     * @return 值
     * @throws SQLException SQL异常
     */
    T getResult(ResultSet resultSet,String columnName)throws SQLException;
}
