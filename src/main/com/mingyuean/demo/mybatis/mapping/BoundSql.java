package com.mingyuean.demo.mybatis.mapping;

import java.util.Map;

/**
 * @author MingYueAn
 * <p>  绑定的SQL, 是从SqlSource而来，将动态内容都处理完成得到的SQL语句字符串，其中包括?,还有绑定的参数
 * <p>  2023/3/23 16:27
 * @version: 1.0
 */
public class BoundSql {

    /**
     * 返回值类型的全限定类名
     */
    private String resultType;
    /**
     * 参数类型的全限定类名
     */
    private String parameterType;
    /**
     * 用于标识动态SQL中的某个参数占位符的位置及其对应的属性名
     */
    private Map<Integer, String> parameterMap;
    /**
     * SQL语句
     */
    private String sql;

    public BoundSql(String resultType, String parameterType, Map<Integer, String> parameterMap, String sql) {
        this.resultType = resultType;
        this.parameterType = parameterType;
        this.parameterMap = parameterMap;
        this.sql = sql;
    }

    public String getResultType() {
        return resultType;
    }

    public String getParameterType() {
        return parameterType;
    }

    public Map<Integer, String> getParameterMap() {
        return parameterMap;
    }

    public String getSql() {
        return sql;
    }
}
