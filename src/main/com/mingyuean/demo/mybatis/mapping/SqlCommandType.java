package com.mingyuean.demo.mybatis.mapping;

import org.omg.CORBA.UNKNOWN;

/**
 * @author MingYueAn
 * <p>  SQL命令类型
 * <p>  2023/3/20 14:05
 * @version: 1.0
 */
public enum SqlCommandType {

    /**
     * 未知
     */
    UNKNOWN,
    /**
     * 插入
     */
    INSERT,
    /**
     * 更新
     */
    UPDATE,
    /**
     * 删除
     */
    DELETE,
    /**
     * 查找
     */
    SELECT;
}
