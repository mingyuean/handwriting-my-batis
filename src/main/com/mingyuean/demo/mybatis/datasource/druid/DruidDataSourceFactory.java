package com.mingyuean.demo.mybatis.datasource.druid;

import com.alibaba.druid.pool.DruidDataSource;
import com.mingyuean.demo.mybatis.datasource.DataSourceFactory;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author MingYueAn
 * <p>  Druid数据源工厂
 * <p>  2023/3/22 20:47
 * @version: 1.0
 */
public class DruidDataSourceFactory implements DataSourceFactory {

    /**
     * 属性列表
     */
    private Properties properties;

    /**
     * 获取 Druid 数据源的方法，根据属性列表中的配置信息创建数据源对象。
     *
     * @return 返回 Druid 数据源对象。
     */
    @Override
    public DataSource getDataSource() {
        final DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(properties.getProperty("driver"));
        druidDataSource.setUrl(properties.getProperty("url"));
        druidDataSource.setUsername(properties.getProperty("username"));
        druidDataSource.setPassword(properties.getProperty("password"));
        return druidDataSource;
    }

    /**
     * 设置属性配置信息的方法，用于设置属性列表。
     *
     * @param properties 需要设置的 Properties 参数。
     * @return 返回设置后的 Properties 对象。
     */
    @Override
    public Properties setProperties(Properties properties) {
        return this.properties = properties;
    }
}
