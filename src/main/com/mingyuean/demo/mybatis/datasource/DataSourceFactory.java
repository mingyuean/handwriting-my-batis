package com.mingyuean.demo.mybatis.datasource;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author MingYueAn
 * <p>  数据源工厂
 * <p>  2023/3/22 21:06
 * @version: 1.0
 */
public interface DataSourceFactory {
    /**
     * 获取 DataSource 对象的方法。
     *
     * @return 返回 DataSource 对象。
     */
    DataSource getDataSource();

    /**
     * 设置 Properties 配置信息的方法。
     *
     * @param properties 需要设置的 Properties 参数。
     * @return 返回设置后的 Properties 对象。
     */
    Properties setProperties(Properties properties);
}
