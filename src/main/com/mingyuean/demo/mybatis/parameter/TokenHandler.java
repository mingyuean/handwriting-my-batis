package com.mingyuean.demo.mybatis.parameter;

/**
 * @author MingYueAn
 * <p>  令牌处理程序
 * <p>  2023/3/12 17:17
 * @version: 1.0
 */
public interface TokenHandler {
    /**
     * 处理
     * @param value 值
     * @return ?
     */
    String handleToken(String value);
}
