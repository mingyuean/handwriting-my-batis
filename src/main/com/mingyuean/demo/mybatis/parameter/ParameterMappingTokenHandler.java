package com.mingyuean.demo.mybatis.parameter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MingYueAn
 * <p>  参数映射令牌处理程序
 * <p>  2023/3/12 19:47
 * @version: 1.0
 */
public class ParameterMappingTokenHandler implements TokenHandler {

    private final List<ParameterMapping> parameterMappings = new ArrayList<>();

    @Override
    public String handleToken(String content) {
        parameterMappings.add(new ParameterMapping(content));
        return "?";
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }
}
