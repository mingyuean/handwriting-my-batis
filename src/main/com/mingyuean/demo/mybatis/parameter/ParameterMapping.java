package com.mingyuean.demo.mybatis.parameter;

/**
 * @author MingYueAn
 * <p>  参数映射
 * <p>  2023/3/12 19:54
 * @version: 1.0
 */
public class ParameterMapping {
    private String content;

    @Override
    public String toString() {
        return "ParameterMapping{" +
                "content='" + content + '\'' +
                '}';
    }

    public ParameterMapping(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
