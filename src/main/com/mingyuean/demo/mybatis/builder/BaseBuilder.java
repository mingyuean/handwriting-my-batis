package com.mingyuean.demo.mybatis.builder;

import com.mingyuean.demo.mybatis.TypeAliasRegistry;
import com.mingyuean.demo.mybatis.session.Configuration;

/**
 * @author MingYueAn
 * <p>  抽象基础构建器类
 * <p>  2023/3/20 9:39
 * @version: 1.0
 */
public abstract class BaseBuilder {
    /**
     * 配置信息
     */
    protected final Configuration configuration;

    /**
     * 类型别名注册机
     */
    protected final TypeAliasRegistry typeAliasRegistry;

    /**
     * 构造方法，传入配置信息
     *
     * @param configuration 配置信息
     */
    protected BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        // 从配置信息中获取类型比别名注册机
        this.typeAliasRegistry = this.configuration.getTypeAliasRegistry();
    }

    /**
     * 获取配置信息
     *
     * @return 配置信息
     */
    public Configuration getConfiguration() {
        return configuration;
    }
}
