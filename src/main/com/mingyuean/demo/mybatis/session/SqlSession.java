package com.mingyuean.demo.mybatis.session;

/**
 * @author MingYueAn
 * <p>
 * <p>  2023/3/15 13:45
 * @version: 1.0
 */
public interface SqlSession {
    /**
     * 获取指定Mapper接口的实例，用于执行相应的SQL操作。
     *
     * @param mapperClass Mapper接口类对象
     * @param <T>         接口类型
     * @return 接口的实例
     */
    <T> T getMapper(Class<T> mapperClass);

    /**
     * 获取Configuration对象，该对象包含了MyBatis的所有配置信息。
     *
     * @return Configuration对象
     */
    Configuration getConfiguration();


    /**
     * 根据传入的命名空间ID以及参数，执行查询并返回结果
     *
     * @param namespaceId 查询语句的命名空间ID
     * @param parameter 查询语句在执行时需要的参数
     * @param <T> 任意泛型类型，用来指定返回的结果类型，例如：User、List<User> 等
     * @return 返回查询结果，类型由泛型参数 T 指定
     */
    <T> T selectOne(String namespaceId, Object parameter);
}
