package com.mingyuean.demo.mybatis.session;

import com.mingyuean.demo.mybatis.builder.xml.XMLConfigBuilder;
import com.mingyuean.demo.mybatis.session.defaults.DefaultSqlSessionFactory;

import java.io.InputStream;

/**
 * @author MingYueAn
 * <p>  build创建SqlSessionFactory
 * <p>  2023/3/17 9:15
 * @version: 1.0
 */
public class SqlSessionFactoryBuilder {

    /**
     * <p>1. 解析配置文件，封装容器对象
     * <p>2. 创建SqlSessionFactory工厂对象
     *
     * @param inputStream 配置文件输入流
     * @return SqlSessionFactory工厂对象
     */
    public SqlSessionFactory build(InputStream inputStream) {
        // 使用XMLConfigBuilder:专门解析核心配置文件的解析类
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(inputStream);
        // 开始解析，返回全局配置类
        Configuration configuration = xmlConfigBuilder.parse();
        // 创建SqlSessionFactory工厂对象
        return build(configuration);
    }

    /**
     * 创建SqlSessionFactory工厂对象
     *
     * @param configuration 配置文件的JavaBean
     * @return SqlSessionFactory工厂对象
     */
    private SqlSessionFactory build(Configuration configuration) {
        return new DefaultSqlSessionFactory(configuration);
    }
}
