package com.mingyuean.demo.mybatis.session;

/**
 * @author MingYueAn
 * <p>
 * <p>  2023/3/15 13:45
 * @version: 1.0
 */
public interface SqlSessionFactory {
    /**
     * 打开一个SqlSession
     * @return SqlSession
     */
    SqlSession openSession();
}
