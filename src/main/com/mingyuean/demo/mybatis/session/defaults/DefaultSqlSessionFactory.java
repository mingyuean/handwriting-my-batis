package com.mingyuean.demo.mybatis.session.defaults;

import com.mingyuean.demo.mybatis.session.Configuration;
import com.mingyuean.demo.mybatis.session.SqlSession;
import com.mingyuean.demo.mybatis.session.SqlSessionFactory;

/**
 * @author MingYueAn
 * <p>
 * <p>  2023/3/15 13:47
 * @version: 1.0
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private final Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
