package com.mingyuean.demo.mybatis.session;

import java.sql.Connection;

/**
 * @author MingYueAn
 * <p>  TransactionIsolationLevel枚举，定义了事务隔离级别对应的常量值。
 * <p>  2023/3/22 21:18
 * @version: 1.0
 */
public enum TransactionIsolationLevel{

    /**
     * NONE 隔离级别，表示不支持事务。
     */
    NONE(Connection.TRANSACTION_NONE),

    /**
     * READ_COMMITTED 隔离级别，表示一个事务只能读取另一个事务已经提交的数据。防止脏读和幻读，但可能出现不可重复读。
     */
    READ_COMMITTED(Connection.TRANSACTION_READ_COMMITTED),

    /**
     * READ_UNCOMMITTED 隔离级别，表示一个事务可以读取另一个事务尚未提交的数据。允许出现脏读、不可重复读和幻读问题。
     */
    READ_UNCOMMITTED(Connection.TRANSACTION_READ_UNCOMMITTED),

    /**
     * REPEATABLE_READ 隔离级别，表示一个事务在多次读取同一数据时，能够保证其读取的数据是一致的。防止脏读、不可重复读，但可能出现幻读问题。
     */
    REPEATABLE_READ(Connection.TRANSACTION_REPEATABLE_READ),

    /**
     * SERIALIZABLE 隔离级别，最高的隔离级别，强制事务串行执行，避免了脏读、不可重复读和幻读问题，但并发性能低，一般不推荐使用。
     */
    SERIALIZABLE(Connection.TRANSACTION_SERIALIZABLE);

    /**
     * 隔离级别对应的整型数值。
     */
    private final int level;

    /**
     * 构造函数，初始化隔离级别对应的整型数值。
     *
     * @param level 隔离级别对应的整型数值。
     */
    TransactionIsolationLevel(int level) {
        this.level = level;
    }

    /**
     * 获取隔离级别的整型数值。
     *
     * @return 返回隔离级别对应的整型数值。
     */
    public int getLevel() {
        return level;
    }
}