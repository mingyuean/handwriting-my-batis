package com.mingyuean.demo.mybatis.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author MingYueAn
 * <p>  Transaction接口，定义了事务操作的基本方法。
 * <p>  2023/3/22 20:16
 * @version: 1.0
 */
public interface Transaction {

    /**
     * 获取事务关联的数据库连接对象。
     *
     * @return 返回与事务关联的数据库连接对象。
     * @throws SQLException 如果发生 SQL 异常，则抛出 SQLException 异常。
     */
    Connection getConnection() throws SQLException;

    /**
     * 提交事务。
     *
     * @throws SQLException 如果发生 SQL 异常，则抛出 SQLException 异常。
     */
    void commit() throws SQLException;

    /**
     * 回滚事务。
     *
     * @throws SQLException 如果发生 SQL 异常，则抛出 SQLException 异常。
     */
    void rollback() throws SQLException;

    /**
     * 关闭事务。
     *
     * @throws SQLException 如果发生 SQL 异常，则抛出 SQLException 异常。
     */
    void close() throws SQLException;
}
