package com.mingyuean.demo.mybatis.transaction.jdbc;

import com.mingyuean.demo.mybatis.session.TransactionIsolationLevel;
import com.mingyuean.demo.mybatis.transaction.Transaction;
import com.mingyuean.demo.mybatis.transaction.TransactionFactory;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * @author MingYueAn
 * <p>  Jdbc事务工厂
 * <p>  2023/3/22 20:48
 * @version: 1.0
 */
public class JdbcTransactionFactory implements TransactionFactory {
    @Override
    public Transaction newTransaction(Connection connection) {
        return new JdbcTransaction(connection);
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new JdbcTransaction(dataSource,level,autoCommit);
    }
}
