package com.mingyuean.demo.mybatis.transaction;

import com.mingyuean.demo.mybatis.session.TransactionIsolationLevel;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * @author MingYueAn
 * <p>  TransactionFactory 接口，用于定义创建事务对象的方法。
 * <p>  2023/3/22 20:13
 * @version: 1.0
 */
public interface TransactionFactory {
    /**
     * 基于连接创建新事务对象的方法。
     * @param connection 数据库连接对象。
     * @return 返回新的事务对象。
     */
    Transaction newTransaction(Connection connection);
    /**
     * 基于数据源、隔离级别和自动提交标志创建新事务对象的方法。
     * @param dataSource 数据源对象。
     * @param level 隔离级别。
     * @param autoCommit 自动提交标志。
     * @return 返回新的事务对象。
     */
    Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit);
}
