package com.mingyuean.demo.mybatis;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author MingYueAn
 * <p>  类型别名注册表
 * <p>  2023/3/22 19:57
 * @version: 1.0
 */
public class TypeAliasRegistry {

    /**
     * 用于存储类型别名的映射关系的 map
     */
    private final Map<String, Class<?>> TYPE_ALIAS_MAP = new HashMap<>();

    /**
     * 构造方法，初始化一些基本的类型别名映射关系
     */
    public TypeAliasRegistry() {
        registerAlias("string", String.class)
                .registerAlias("byte", Byte.class)
                .registerAlias("long", Long.class).
                registerAlias("short", Short.class).
                registerAlias("int", Integer.class).
                registerAlias("integer", Integer.class).
                registerAlias("double", Double.class).
                registerAlias("float", Float.class).
                registerAlias("boolean", Boolean.class);
    }

    /**
     * 注册二者类型之间的别名。
     *
     * @param alias 别名
     * @param value 类型值
     * @return 返回该 TypeAliasRegistry 对象
     */
    public TypeAliasRegistry registerAlias(String alias, Class<?> value) {
        // 将别名转换为小写
        final String key = alias.toLowerCase(Locale.ENGLISH);
        // 将别名和类型值存入到 map 中
        TYPE_ALIAS_MAP.put(key, value);
        return this;
    }

    /**
     * 解析给定的类型别名所表示的类。
     *
     * @param alias 类型别名
     * @return 返回解析出来的类型，如果不存在则返回 null
     */
    public <T> Class<T> resolveAlias(String alias) {
        // 将别名转换为小写
        final String key = alias.toLowerCase(Locale.ENGLISH);
        // 从 map 中取出指定别名所对应的类
        return (Class<T>) TYPE_ALIAS_MAP.get(key);
    }
}
