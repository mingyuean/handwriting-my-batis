package test1;

import com.mingyuean.demo.mybatis.builder.xml.XMLConfigBuilder;
import com.mingyuean.demo.mybatis.io.Resources;
import com.mingyuean.demo.mybatis.session.Configuration;
import com.mingyuean.demo.mybatis.session.SqlSession;
import com.mingyuean.demo.mybatis.session.SqlSessionFactory;
import com.mingyuean.demo.mybatis.session.SqlSessionFactoryBuilder;
import com.mingyuean.demo.mybatis.session.defaults.DefaultSqlSession;
import com.mingyuean.demo.test.User;
import com.mingyuean.demo.test.UserMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author MingYueAn
 * <p>
 * <p>  2023/3/13 14:39
 * @version: 1.0
 */
public class MyTest {
    private static final Logger log = LoggerFactory.getLogger(MyTest.class);

    @Test
    public void test1() throws IOException {
        // 根据配置文件的路径，加载成字节输入流，存入内存，此时配置文件未解析
        final InputStream inputStream = Resources.getResourceAsStream("mybatis_config.xml");
        // 解析全局配置文件
        final SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        final SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取映射器对象
        final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        // 调用 UserMapper 接口的 findUserByUsernamePassword 方法，并输出结果
        final User user = userMapper.findUserById(1);
        System.out.println("user = " + user);
    }

    @Test
    public void test2() throws IOException {
        // 根据配置文件的路径，加载成字节输入流，存入内存，此时配置文件未解析
        final InputStream inputStream = Resources.getResourceAsStream("mybatis_config.xml");
        // 解析 XML
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(inputStream);
        Configuration configuration = xmlConfigBuilder.parse();

        // 获取 DefaultSqlSession
        SqlSession sqlSession = new DefaultSqlSession(configuration);

        Object[] req = {1L};
        Object res = sqlSession.selectOne("com.mingyuean.demo.test.UserMapper.findUserById", req);
        log.debug("res = {}", res);
    }
}
